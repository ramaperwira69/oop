<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');
$sheep = new Animal("shaun");

echo "Name = " . $sheep->name . "<br>"; // "shaun"
echo "Legs = " . $sheep->legs . "<br>"; // 2
echo "Cold Blooded = " . $sheep->cold_blooded . "<br>"; // false
echo "<br>";

$frog = new Frog("buduk");

echo "Name = " . $frog->name . "<br>"; // "shaun"
echo "Legs = " . $frog->legs . "<br>"; // 2
echo "Cold Blooded = " . $frog->cold_blooded . "<br>"; // false
$frog->jump(); 
echo "<br>";
echo "<br>";


$ape = new Ape("kera-sakti");

echo "Name = " . $ape->name . "<br>"; // "shaun"
echo "Legs = " . $ape->legs . "<br>"; // 2
echo "Cold Blooded = " . $ape->cold_blooded . "<br>"; // false
$ape->yell(); 
?>

